# Changelog
Uzta realeases follow semantic versioning convention.

## 2.0.4 - 2015-10-29
* Fixed: Uzta parses maximum 16 photos of an album.
* Fixed: Uzta was checking after a localized string, even if the operation was unnecesary.

## 2.0.2 - 2015-10-26
* Fixed: improper use of nested OrderedDict was causing indesirable output for discussions.

## 2.0.1 - 2015-10-26
* Fixed: typo error on pages reader was returning undesired content.

## 2.0.0 - 2015-10-24
* Implemented: now writing standard yaml output

## 1.3.6 - 2015-10-21
* Fixed: compatibility with uzta-web after dropping wfnc feature.

## 1.3.5 - 2015-10-21
* Added debug mode

## 1.2.5 - 2015-10-21
* Fixed: support new Elgg versions.

## 1.2.4 - 2015-10-07
* Added: now creating README.md in the output.
* Fixed: website variable was used but not defined in photos readinng function.
* Fixed: username variable was used but not defined in wires reading function.
* Fixed: output dir name parameter was missing in function call.

## 1.1.1 - 2015-10-07 [JANKED]
* Added: `uzta` function so it can be imported in other modules.
* Fixed: hardcoded url in `slurp_links` function.

## 1.0.0 - 2015-10-05
* First release
